## Intermediate Variables

#### The problem

Here is a backend view function in a web framework:

```
def get_jcl_info(self, jcl):
    data = {}
    desc = {"name": "name",
            "system": "system",
            "description": "description",
            "script": "script",
            "applications": "jcl_applications",
            "predecessors": "jcl_predecessors",
            "successors": "jcl_successors"}

    for item in desc.keys():
        extracted = self.config.jcl_jobs[jcl].__dict__[item]
        data[desc[item]] = extracted[:] if isinstance(extracted, list) else extracted

    data["last_executions"] = self.config.jcl_jobs[jcl].executions.get_executions()
    if self.config.jcl_jobs[jcl].executable:
        data["processing_script"] = self.config.jcl_jobs[jcl].executable.get("processing_script")
        data["bm_task"] = self.config.jcl_jobs[jcl].executable.get("bm_task")
        data["job"] = self.config.jcl_jobs[jcl].executable.get("job")
        data["desk"] = self.config.jcl_jobs[jcl].executable.get("desk")
        data["nickname"] = self.config.jcl_jobs[jcl].executable.get("nickname")
        data["group"] = self.config.jcl_jobs[jcl].executable.get("group")

    return data
```

Issues I have with this code are:


1. There are too many dict lookups which mangle the code.
2. Function is too long. Should have 4-5 lines.
3. There are reduntant statements, which mangle the code too.
4. Implicit iteration through attributes using ```__dict__```.


#### Issue #1: Too many dict lookups

We have a lot of ```self.config.jcl_jobs[jcl]``` in the code below.

```
data["last_executions"] = self.config.jcl_jobs[jcl].executions.get_executions()
if self.config.jcl_jobs[jcl].executable:
    data["processing_script"] = self.config.jcl_jobs[jcl].executable.get("processing_script")
    data["bm_task"] = self.config.jcl_jobs[jcl].executable.get("bm_task")
    data["job"] = self.config.jcl_jobs[jcl].executable.get("job")
    data["desk"] = self.config.jcl_jobs[jcl].executable.get("desk")
    data["nickname"] = self.config.jcl_jobs[jcl].executable.get("nickname")
    data["group"] = self.config.jcl_jobs[jcl].executable.get("group")
```

You might be tempted to “save” a variable name and only use ```self.config.jcl_jobs[jcl]``` as a good idea. Weeell… it is definitely NOT a SCALABLE idea, that is for sure. What do I have in mind?

Everything in Python is based on some kind of mapping like dictionaries. So when you get an attribute from an object then the ```__dict__[attr_name]``` is being called. So you might assume that reading from dictionaries directly is not a mistake. But let’s calculate the computational complexity for this ```self.config.jcl_jobs[jcl].executions.get_executions()```

1. Search ‘config’ attribute from self
2. Search ‘jcl_jobs’ attribute from config
3. Search ‘jcl’ key from jcl_jobs dict
4. Search ‘executions' from jcl_obj
5. Search ‘get_executions’ method from executions.

The time complexity for hash tables is O(1) to O(n) with average of O(log n) which depends on speed of growth of elements in it. We get a 0(1) complexity for static key dictionaries like searching attributes in objects based on the same class. So the only concern for scalability refers to point 3: ```Search ‘jcl’ key from jcl_jobs dict``` and the overall complexity is O(1) + O(1) + O(log n3) + O(1) + O(1) = O(log n3) + 4xO(1). Using a simple variable will speed up the code 8 times when searching for that object.


```
jcl_job = self.config.jcl_jobs[jcl]
data["last_executions"] = jcl_job.executions.get_executions()
if jcl_job.executable:
    data["processing_script"] = jcl_job.executable.get("processing_script")
    data["bm_task"] = jcl_job.executable.get("bm_task")
    data["job"] = jcl_job.executable.get("job")
    data["desk"] = jcl_job.executable.get("desk")
    data["nickname"] = jcl_job.executable.get("nickname")
    data["group"] = jcl_job.executable.get("group")
```


And now we can even cut the “executable” attribute:


```
jcl_job = self.config.jcl_jobs[jcl]
data["last_executions"] = jcl_job.executions.get_executions()
executable = jcl_job.executable
if executable:
    data["processing_script"] = executable.get("processing_script")
    data["bm_task"] = executable.get("bm_task")
    data["job"] = executable.get("job")
    data["desk"] = executable.get("desk")
    data["nickname"] = executable.get("nickname")
    data["group"] = executable.get("group")
```


#### Issue #2: Function is too long.

A dict literal can be treated as 1 line. We can save several lines by changing this:


``` 
    data = {}
    data["processing_script"] = self.config.jcl_jobs[jcl].executable.get("processing_script")
    data["bm_task"] = self.config.jcl_jobs[jcl].executable.get("bm_task")
    data["job"] = self.config.jcl_jobs[jcl].executable.get("job")
    data["desk"] = self.config.jcl_jobs[jcl].executable.get("desk")
    data["nickname"] = self.config.jcl_jobs[jcl].executable.get("nickname")
    data["group"] = self.config.jcl_jobs[jcl].executable.get("group")
```


to this:


```
    data = {
        "processing_script": executable.get("processing_script"),
        "bm_task": executable.get("bm_task"),
        "job": executable.get("job"),
        "desk": executable.get("desk"),
        "nickname": executable.get("nickname"),
        "group": executable.get("group")
    }
```


#### Issue #3: Mangled code:


```
data[desc[item]] = extracted[:] if isinstance(extracted, list) else extracted
```


1. Never use ```isinstance()``` in the code unless you are creating a test or a validator. Using it is against duck-typing and understanding polymorphism.
2. Cleanup reduntant statements: This ```_temp = extracted[:] if isinstance(extracted, list) else extracted``` is just ```_temp = extracted```

#### Issue #4: The ```__dict__``` iteration.

Let's exactly examine these lines:


```
    data = {}
    desc = {"name": "name",
            "system": "system",
            "description": "description",
            "script": "script",
            "applications": "jcl_applications",
            "predecessors": "jcl_predecessors",
            "successors": "jcl_successors"}

    for item in desc.keys():
        extracted = self.config.jcl_jobs[jcl].__dict__[item]
        data[desc[item]] = extracted[:] if isinstance(extracted, list) else extracted
```


1. There is a ```desc``` mapping which translates attribute names from the ```self.config.jcl_jobs[jcl]``` which we already have changed to ```jcl_job```.

    * purpose: I assume that the ```jcl_job``` was meant to be a view on it's own or the ```desc``` mapping is used to detach the object attribute names from the view.
    
2. Issues with error types: You should use ```getattr()``` becauase you need and AttributeError in case an attribute does not exist. You get only a KeyError when using ```__dict__[item]```.


   
Let's cut this mapping and just use a dict literal:


```
def get_jcl_info(self, jcl):
    jcl_job = self.config.jcl_jobs[jcl]
    data = {"name": jcl_job.name,
            "system": jcl_job.system,
            "description": jcl_job.description,
            "script": jcl_job.script,
            "applications": jcl_job.jcl_applications,
            "predecessors": jcl_job.jcl_predecessors,
            "successors": jcl_job.jcl_successor,
            "last_executions": jcl_job.executions.get_executions()}

    executable = jcl_job.executable
    if executable:
        exec_data = {
            "processing_script": executable.get("processing_script"),
            "bm_task": executable.get("bm_task"),
            "job": executable.get("job"),
            "desk": executable.get("desk"),
            "nickname": executable.get("nickname"),
            "group": executable.get("group")
        }
        data.update(exec_data)
    return data
```

#### Final thoughts

If we treat dict literals as 1 line, then we have 6 lines without the “return” statement. So we have to think if we want to remove the executable = jcl_job.executable line and get back to the jcl_job.executable standard. The list of attributes for JclJob object is small, static and probably it will evaluate only to O(1).


```
def get_jcl_info(self, jcl):
    jcl_job = self.config.jcl_jobs[jcl]
    data = {"name": jcl_job.name,
            "system": jcl_job.system,
            "description": jcl_job.description,
            "script": jcl_job.script,
            "applications": jcl_job.jcl_applications,
            "predecessors": jcl_job.jcl_predecessors,
            "successors": jcl_job.jcl_successor,
            "last_executions": jcl_job.executions.get_executions()}

    if jcl_job.executable:
        exec_data = {
            "processing_script": jcl_job.executable.get("processing_script"),
            "bm_task": jcl_job.executable.get("bm_task"),
            "job": jcl_job.executable.get("job"),
            "desk": jcl_job.executable.get("desk"),
            "nickname": jcl_job.executable.get("nickname"),
            "group": jcl_job.executable.get("group")
        }
        data.update(exec_data)
    return data
```


Or maybe we should think about separation of concern concept? It applies to anything, even to functions:


```
class Whatever:

    def get_jcl_info(self, jcl):
        jcl_job = self.config.jcl_jobs[jcl]
        data = {"name": jcl_job.name,
                "system": jcl_job.system,
                "description": jcl_job.description,
                "script": jcl_job.script,
                "applications": jcl_job.jcl_applications,
                "predecessors": jcl_job.jcl_predecessors,
                "successors": jcl_job.jcl_successor,
                "last_executions": jcl_job.executions.get_executions()}

        exec_data = self._get_jcl_exec_dict(jcl_job.executable)
        data.update(exec_data)
        return data

    @staticmethod
    def _get_jcl_exec_dict(executable) -> dict:
        if executable:
            exec_data = {
                "processing_script": executable.get("processing_script"),
                "bm_task": executable.get("bm_task"),
                "job": executable.get("job"),
                "desk": executable.get("desk"),
                "nickname": executable.get("nickname"),
                "group": executable.get("group")
            }
            return exec_data
        return {}
```


Maybe you have a feeling that that updating of a dictionary with an empty dictionary is a waste of time… probably, but on the other hand we keep the return type stable which will help us in the long run. 

The time of development is more important than the time of execution of the code. And I have a feeling that writing the refactored code would take less time than the original version.

