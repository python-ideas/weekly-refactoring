## Factory pattern

#### The problem

We've refactored a complex if-elif-else statement into a polimoprhic OOP model. It has been separated into:

1. Abstract class
2. Subclasses
3. Function helping with object instantiation

We would like to have a generic way of creating Parameter objects without importing each class individualy e.g. 

```
def Parameter(*args, **kwargs) -> AbstractParameter:
    ...
```


### Possible approaches


1. We can use a simple if-else statement making conditions for each variation of arguments.
2. We can make a self-discovery mechanism using ```AbstractParameter.__subclasses__()``` method.

#### The if-else approach


In the original code (before refactoring) We've analyzed this structure:


```
...
if param_obj.parameter_type == 'double' and param_obj.label_of_data:
    ...
elif param_obj.parameter_type == 'double': 
    ...
elif param_obj.parameter_type == 'string': 
    ...
elif param_obj.parameter_type == 'other': 
    ...
```

This can be the bases for our Factory:

```
from .subclasses import StringParameter, DoubleParameter, OtherParameter

def Parameter(name, value, parameter_type) -> AbstractParameter:
    if parameter_type == 'double': 
        return DoubleParameter(name, value)
    elif parameter_type == 'string': 
        return StringParameter(name, value)
    elif parameter_type == 'other': 
        return OtherParameter(name, value)
    else:
        return None
```

 
    

