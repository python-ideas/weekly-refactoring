# Python type hinting

### A Simple way to help your IDE to help you:)

We have an inhouse library which simplifies our database connections in our code. The problem here is that the IDE does not know what type is the *cs* object.

![Type Hinting](img/hint_1.png)

We know that is a *pyodbc.Cursor* object so we can suggest to our project what to do. We need to go to the code of our context manager:

![Type Hinting](img/hint_2.png)

The *\_\_enter\_\_* method returns a cursor, wchich comes from *self.get_connection(...)*. We can just **hint** the method that should always return the *pyodbc.Cursor* thus revealing the API to the IDE:

![Type Hinting](img/hint_3.png)

And now we see *pyodbc.Cursor* API:

![Type Hinting](img/hint_4.png)

### Simplifying your refactoring

Let's say We get some for-loop in a method of a 1000-2000 line class. 

![Type Hinting](img/hint_5.png)

 We know what type the *job* object is but we don't want to jump between files to get the proper API. We can make an **inline hint**:

![Type Hinting](img/hint_6.png)

But we can also do a hint without any other code:

![Type Hinting](img/hint_7.png)

Lastly there is the most often use of hints: directly in method signature:

![Type Hinting](img/hint_8.png)


### A simple hack

Hints are mandatory because they're a part of the documentation but a too complicated hint probably means that you have to refactor your code:

* Dict[_U, List[_T]],
* (Type[Any], str, Tuple[Any], Dict[Any, Any]) -> Any
* List[Tuple[str, str]]) -> Dict[str, set]


